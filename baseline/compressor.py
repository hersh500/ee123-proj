import numpy as np
import matplotlib.pyplot as plt
from scipy import misc
import scipy.signal as signal
import pywt
import bitarray
import struct
#import gzip
import lzma as gzip

d_97_lp_a = np.array(
            [0.026748757, 
            -0.016864118,
            -0.078223266,
             0.266864118,
             0.609294901,
             0.266864118, 
            -0.078223266,
            -0.016864118,
             0.026748757])

d_97_hp_a = np.array(
            [0,
             0.091271763, 
            -0.057543526,
            -0.591271763,
             1.115087052,
            -0.591271763,
            -0.057543526,
             0.091271763,
             0])

d_97_hp_s = np.array(
            [0.026748757, 
             0.016864118,
            -0.078223266,
            -0.266864118,
             0.609294901,
            -0.266864118, 
            -0.078223266,
             0.016864118,
             0.026748757])

d_97_lp_s = np.array(
            [0,
            -0.091271763, 
            -0.057543526,
             0.591271763,
             1.115087052,
             0.591271763,
            -0.057543526,
            -0.091271763,
            0])
coeffs1 = [0.001, 0.09, 0.4, 0.8]
coeffs = [0.001, 0.095, 0.4, 0.8]
filter_bank = [d_97_lp_a, d_97_hp_a, d_97_lp_s, d_97_hp_s]
#cdf97 = pywt.Wavelet('cdf97', filter_bank=filter_bank)
cdf97 = pywt.Wavelet('db4')

def get_image_comp(image):
    return image[:,:,0], image[:,:,1], image[:,:,2]


def getYCbCr(im):
    xform = np.array([[.299, .587, .114], [-.1687, -.3313, .5], [.5, -.4187, -.0813]])
    ycbcr = im.dot(xform.T)
    ycbcr[:,:,[1,2]] += 128
    return np.uint8(ycbcr)


def getRGB(im):
    xform = np.array([[1, 0, 1.402], [1, -0.34414, -.71414], [1, 1.772, 0]])
    rgb = im.astype(np.float)
    rgb[:,:,[1,2]] -= 128
    rgb = rgb.dot(xform.T)
    np.putmask(rgb, rgb > 255, 255)
    np.putmask(rgb, rgb < 0, 0)
    return np.uint8(rgb)


def calc_depth(shape):
    shape = (shape[0], shape[1])
    return int(np.log2(min(shape)/(len(cdf97.filter_bank[0])*4)))


def compress(im, depth=1, out_bits=1, max_bytes=10000, coeff_per=coeffs,
    side_ds=8.0):
    # Top level compression function
    if len(im.shape) == 3:
        dumb_comp = dumb_compressor_color(im.copy(), 8)
        if dumb_comp.length() / 8 < max_bytes:
            print("Compressing Orignal image using GZIP")
            return dumb_comp
        depth = calc_depth(im.shape)
        image_shape = (im.shape[0], im.shape[1])
        bits = bitarray.bitarray()
        im = getYCbCr(im)
        for channel in range(im.shape[2]):
            mat = im[:,:,channel]
            if channel != 0:
                mat = misc.imresize(mat, 1/side_ds)
            mat = mat/255 - 0.5
            print(mat.shape)
            bits.extend(encode(mat, depth, coeff_per = coeff_per, out_bits =
                out_bits))
            print(bits.length())
        extra_bits = 8 - bits.length() % 8
        bits_meta = create_meta_data(image_shape, depth, out_bits, 1,
            extra_bits)
        bits_meta.extend(bits)
        bits_meta.extend([0]*extra_bits)
        return zip_bits(bits_meta) 
    else:
        dumb_comp = dumb_compressor(im, out_bits)
        if dumb_comp.length() / 8 < max_bytes:
            print("Dumb Compression for the win")
            return dumb_comp
        image_shape = (im.shape[0], im.shape[1])
        bits = bitarray.bitarray()
        bits.extend(encode(im / 255 + 0.5, depth, out_bits = out_bits,
            coeff_per = coeffs))
        extra_bits = 8 - bits.length() % 8
        bits_meta = create_meta_data(image_shape, depth, out_bits, 0,
            extra_bits)
        bits_meta.extend(bits)
        bits_meta.extend([0]*extra_bits)
        return zip_bits(bits_meta) 

def zip_bits(bits):
    bits_compressed = bitarray.bitarray()
    bits_compressed.frombytes(\
        gzip.compress(bits.tobytes()))
    return bits_compressed


def unzip_bits(compressed_bits):
    bits = bitarray.bitarray()
    bits.frombytes(\
        gzip.decompress(compressed_bits.tobytes()))
    return bits

def binary_compressor(im):
    image_shape = (im.shape[0], im.shape[1])
    print(image_shape)
    bits = bitarray.bitarray()
    bits.extend((im > 128).flatten().tolist())
    print(bits.length(), (im > 128).flatten().shape, im.flatten().shape,
        im.shape)
    extra_bits = 8 - bits.length() % 8
    bits_meta = create_meta_data(image_shape, 0, 1, 0, extra_bits)
    bits_meta.extend(bits)
    bits_meta.extend([0]*extra_bits)
    return zip_bits(bits_meta)

def binary_decompressor(bits_compressed):
    bits = unzip_bits(bits_compressed)
    meta_data, image_bits = get_meta_data(bits)
    image_shape = meta_data[0]
    bx = image_shape[0]
    by = image_shape[1]
    extra_bits = meta_data[4]
    image_bits = image_bits[:-extra_bits] 
    block = np.packbits(\
        np.array(image_bits.tolist())\
        .reshape((bx, by, 1)), axis=-1).reshape(bx, by)
    return np.uint8(block.astype(np.float32)*255 / 128)


def dumb_compressor_color(im, out_bits):
    image_shape = (im.shape[0], im.shape[1])
    bits = bitarray.bitarray()
    for i in range(3):
        bits.extend(dumb_encoder(im[:,:,i], out_bits))
    extra_bits = 8 - bits.length() % 8
    bits_meta = create_meta_data(image_shape, 0, out_bits, 1,
        extra_bits)
    bits_meta.extend(bits)
    bits_meta.extend([0]*extra_bits)
    bits_compressed = bitarray.bitarray()
    bits_compressed.frombytes(\
        gzip.compress(bits_meta.tobytes()))
    return bits_compressed 
        

def dumb_compressor(im, out_bits):
    image_shape = (im.shape[0], im.shape[1])
    bits = dumb_encoder(im, out_bits)
    extra_bits = 8 - bits.length() % 8
    bits_meta = create_meta_data(image_shape, 0, out_bits, 0,
        extra_bits)
    bits_meta.extend(bits)
    bits_meta.extend([0]*extra_bits)
    bits_compressed = bitarray.bitarray()
    bits_compressed.frombytes(\
        gzip.compress(bits_meta.tobytes()))
    return bits_compressed


def dumb_encoder(im, out_bits):
    bits = bitarray.bitarray()
    bits_arr = np.unpackbits(np.uint8(im.flatten()))
    bits.extend(quantize_bits(bits_arr, start_bits=8,\
        out_bits=out_bits).tolist())
    return bits


def create_meta_data(image_shape, depth, out_bits, color, extra_bits):
    bits = bitarray.bitarray()
    shape_bytes = struct.pack('H', np.uint16(image_shape[0]))
    bits.extend(np.unpackbits(np.uint8(shape_bytes[0])))
    bits.extend(np.unpackbits(np.uint8(shape_bytes[1])))
    shape_bytes = struct.pack('H', np.uint16(image_shape[1]))
    bits.extend(np.unpackbits(np.uint8(shape_bytes[0])))
    bits.extend(np.unpackbits(np.uint8(shape_bytes[1])))
    bits.extend(np.unpackbits(np.uint8(depth)))
    bits.extend(np.unpackbits(np.uint8(out_bits)))
    bits.extend(np.unpackbits(np.uint8(color)))
    bits.extend(np.unpackbits(np.uint8(extra_bits)))
    return bits


def dwt_threshold(block, coeff_per):
    flat = block.flatten()
    flat_sorted = np.sort(np.abs(flat))
    threshold = flat_sorted[int((1-coeff_per)*len(flat))]
    flat[abs(flat) < threshold] = 0
    return flat.reshape(block.shape)


def encode(mat, depth, out_bits=6, wave=cdf97, coeff_per=[1], null_depth=-1):
    # This function takes a 2D mat and computes DWT and outputs
    # bits 
    def helper(img, bits, depth, max_depth):
        if depth == max_depth:
            bits.extend(quantize_mat(img, out_bits, coeff_per=1))
            return 
        coeffs = pywt.dwt2(img, wave)
        print("Coeffs Shape:", coeffs[0].shape, [c.shape for c in coeffs[1]])
        for block in coeffs[1]:
            bits.extend(quantize_mat(block, out_bits,
                coeff_per=coeff_per[depth]))
        return helper(coeffs[0], bits, depth+1, max_depth)

    image_bits = bitarray.bitarray()
    helper(mat, image_bits, 0, depth)
    return image_bits


def quantize_mat(mat, out_bits=None, coeff_per=0.9):
    mat = dwt_threshold(mat, coeff_per)
    maxval = np.max(mat)
    minval = np.min(mat)
    norm =2*max(maxval, -minval)
    minval = -norm/2
    maxval = norm/2
    mat = (mat-minval)/norm*255 
    np.putmask(mat, mat > 255, 255)
    np.putmask(mat, mat < 0, 0)
    min_val_u32 = np.float32(minval).view(np.uint32)
    max_val_u32 = np.float32(maxval).view(np.uint32)
    mat_bits = bitarray.bitarray()
    for byte in struct.pack('I', min_val_u32):
        mat_bits.extend(np.unpackbits(np.uint8(byte)))
    #for byte in struct.pack('I', max_val_u32):
    #    mat_bits.extend(np.unpackbits(np.uint8(byte)))
    bits_arr = np.unpackbits(np.uint8(mat.flatten()))
    mat_bits.extend(quantize_bits(bits_arr, start_bits=8,\
        out_bits=out_bits).tolist())
    return mat_bits


def quantize_bits(bits_arr, start_bits, out_bits):
    if start_bits == out_bits:
        return bits_arr
    else:
        return quantize_bits(np.delete(bits_arr, slice(start_bits-1, None,\
            start_bits)), start_bits-1, out_bits)


def compute_size(x, wave, depth):
    if depth == 0:
        return x
    return compute_size((x + len(wave.filter_bank[0]) - 1)//2, wave, depth-1)


def get_block_min_max(bits):
    block_min = struct.unpack('f', bits[0:32].tobytes())[0]
    #block_max = struct.unpack('f', bits[32:64].tobytes())[0]
    return block_min, -block_min


def bits_to_mat_dwt(bits, bx, by, out_bits):
    block_min, block_max = get_block_min_max(bits)
    mat = bits_to_mat(bits[32:], bx, by, out_bits)
    print(mat.shape)
    norm = block_max - block_min
    return (mat*norm/255+block_min)


def bits_to_mat(bits, bx, by, out_bits):
    block = np.packbits(\
        np.array(bits.tolist())\
        .reshape((bx, by, out_bits)), axis=-1).reshape(bx, by)
    return block


def bits_to_mat_color(bits, bx, by, out_bits=None):
    channel_len = int(bits.length() / 3)
    mat = np.zeros((bx, by, 3))
    for i in range(3):
        mat[:,:,i] = bits_to_mat(bits[channel_len*i:channel_len*(i+1)], bx, by,
            out_bits)
    return mat

def get_meta_data(bits):
    im_shape_x = struct.unpack('H', bits[0:16].tobytes())[0]
    im_shape_y = struct.unpack('H', bits[16:32].tobytes())[0]
    depth = struct.unpack('B', bits[32:40].tobytes())[0]
    out_bits = struct.unpack('B', bits[40:48].tobytes())[0]
    color = struct.unpack('B', bits[48:56].tobytes())[0]
    extra_bits = struct.unpack('B', bits[56:64].tobytes())[0]
    return ((im_shape_x, im_shape_y), depth, out_bits, color, extra_bits), bits[64:]


def decompress(compressed_bits, ds=8.0):
    bits = unzip_bits(compressed_bits)
    meta_data, image_bits = get_meta_data(bits)
    image_shape = meta_data[0]
    depth = meta_data[1]
    out_bits = meta_data[2]
    color = meta_data[3]
    extra_bits = meta_data[4]
    image_bits = image_bits[:-extra_bits] 
    if color:
        if depth == 0:
            return bits_to_mat_color(image_bits, image_shape[0],
                image_shape[1], out_bits)
        channels = []
        channel_bits = image_bits
        for i in range(3):
            if i == 0:
                img, channel_bits = decode(channel_bits, cdf97, image_shape, out_bits,
                    depth)
                channels.append(img)
            else:
                im, channel_bits = decode(channel_bits, cdf97, 
                    (round(image_shape[0] /ds), round(image_shape[1] /ds)),
                    out_bits, depth)
                channels.append(im)
           
        img = np.zeros((image_shape[0], image_shape[1], 3))
        for i in range(3):
            if i is not 0:
                channels[i] = channels[i]*255 + 127
                np.putmask(channels[i], channels[i] > 255, 255)
                np.putmask(channels[i], channels[i] < 0, 0)
                channels[i] = misc.imresize(np.uint8(channels[i]),image_shape)
            else:
                channels[i] = channels[i]*255 + 127
                np.putmask(channels[i], channels[i] > 255, 255)
                np.putmask(channels[i], channels[i] < 0, 0)
            img[:,:,i] = channels[i]
        return getRGB(img)
    else:
        if depth == 0:
           return bits_to_mat(image_bits, image_shape[0], image_shape[1], out_bits)
        return np.uint8((decode(image_bits, cdf97, image_shape, out_bits,
                depth)[0])*255 + 127)


def decode(bits, wave, image_shape, out_bits=6, depth=1):
    print("decode called") 
    def helper(bits, depth, max_depth):
        print("image_shape", image_shape)
        bx = compute_size(image_shape[0], wave, depth+1)
        by = compute_size(image_shape[1], wave, depth+1)
        channel_len = bx*by*out_bits+32#Add the norm bits
        blocks = []
        for i in range(3):
            block_bits = bits[channel_len*i:channel_len*(i+1)]
            blocks.append(bits_to_mat_dwt(block_bits, bx, by, out_bits))
        if depth == max_depth-1:
            rest = bits_to_mat_dwt(bits[3*channel_len:4*channel_len], bx, by,
                out_bits)
            return (pywt.idwt2((rest, blocks), wave)), bits[channel_len*4:]
        rest, rest_bits = helper(bits[channel_len*3:], depth+1, max_depth)
        #It seems wavelet reconstruction returns different sized images
        #Than construction. This code is a hack taken from the pywt src
        if rest.shape[0] == blocks[0].shape[0]+1:
            rest = rest[:-1, :] #Remove the last number from every axis
        if rest.shape[1] == blocks[0].shape[1]+1:
            rest = rest[:, :-1]
        return (pywt.idwt2((rest, blocks), wave)), rest_bits

    return helper(bits, 0, depth)


def downsampled_compress(im, ds=4, out_bits=8, coeffs_per = [0.05, 0.5, 0.5, 0.8]):
    return compress(misc.imresize(im, 1/ds), depth=1, out_bits=8, coeff_per =
        coeffs_per)

def downsampled_decompress(bits, ds=4.0, out_bits=8, coeffs_per = [0.05, 0.5, 0.5, 0.8]):
    return misc.imresize(decompress(bits), ds)

def EE123_psnr(ref, meas, maxVal=255):
    assert np.shape(ref) == np.shape(meas)
    "Test image must match measured image dimensions"
    dif = (ref.astype(float)-meas.astype(float)).ravel()
    mse = np.linalg.norm(dif)**2/np.prod(np.shape(ref))
    psnr = 10*np.log10(maxVal**2.0/mse)
    return psnr


def main():
    images = [
        '../images/midterm.tiff',
        #'../images/Isee.tiff', 
        #'../images/Marconi.tiff', 
        #'../images/pauly.tiff', 
        #'../images/calBlue.tiff',
        #'../images/shinde.tiff'
        ]
    bw = [False, True, True, True, False, False]
    for im_name, b in zip(images, bw):
        if not b:
            im = misc.imread(im_name)
        else: 
            im = misc.imread(im_name, flatten=True)
        if im_name == '../images/shinde.tiff':
            im = im[:, :, 0:3]
        print(im.shape)
        bits = compress(im, depth=1, out_bits=8)
        im2 = decompress(bits)
        psnr = EE123_psnr(im, im2)
        print(im_name, 'Compressed Size: ', bits.length() / 8, 'PSNR: ', psnr)
        plt.figure()
        plt.imshow(im2, cmap='gray')
        plt.figure()
        plt.imshow(im, cmap='gray')
    plt.show()

def main2():
    im = misc.imread('../images/pauly.tiff', flatten=True)
    bits = binary_compressor(im)
    im2 = binary_decompressor(bits)
    plt.imshow(im, cmap='gray')
    plt.figure()
    plt.imshow(im2, cmap='gray')
    psnr = EE123_psnr(im, im2)
    print(im2)
    print('Pauly', 'Compressed Size: ', bits.length() / 8, 'PSNR: ', psnr)
    plt.show()

def main3():
    im = misc.imread('../images/Marconi.tiff', flatten=True)
    bits = downsampled_compress(im, ds = 4.0, out_bits=8,
        coeffs_per=coeffs)
    im2 = downsampled_decompress(bits, ds=4.0)
    plt.imshow(im, cmap='gray')
    plt.figure()
    plt.imshow(im2, cmap='gray')
    psnr = EE123_psnr(im, im2)
    print(im2)
    print('Marconi', 'Compressed Size: ', bits.length() / 8, 'PSNR: ', psnr)
    plt.show()

if __name__ == "__main__":
    main()
