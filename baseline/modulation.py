import numpy as np
from numpy import r_
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure,plot, title,stem, loglog,xlabel,ylabel
import bitarray, time, urllib, ssl
from scipy import signal, integrate
#from math import gcd
from fractions import gcd
from functools import reduce
from numpy import ones,zeros, pi, cos, exp, sign
from scipy import misc
import serial
import sounddevice as sd



def lcm(numbers):
    return reduce(lambda x,y: (x*y)//gcd(x,y), numbers, 1)

def afsk1200(bits, fc=3000, df=600, baud=2400, fs=48000):
    fs_eff = lcm((fs, baud))
    bit_length = fs_eff//baud #Each bit's duration is the sampling rate (samples/s) / (bits/s) => samples/bit
    num_samples = bit_length * len(bits) #The total number of samples is the bit length * # bits
    tot_time = num_samples/fs_eff #converts samples to time using sample rate
    #change the bitstream to be between -1 and 1 instead of 0 and 1
    m = np.zeros(num_samples)
    #Expands the bitstream to its full length, basically inserting bit_length copies of each bit into the stream
    cur_idx = 0
    for bit in bits:
        for i in range(0, bit_length): 
            if bit == 1:
                m[cur_idx] = bit
            if bit == 0 or bit == -1:
                m[cur_idx] = -1
            cur_idx += 1
    t = np.linspace(0, tot_time, num_samples) #time indices for the inte gration
    func = lambda time: m[time]
    integrated = np.array(integrate.cumtrapz(m, t, initial=0)) #integrat es over m to ensure smooth frequency transitions
    sig = np.cos(2*np.pi*fc*t + 2*np.pi*df*integrated) #The actual si gnal is a series of cosines at 1200 Hz and 2200 Hz
    sig = sig[::fs_eff//fs] 
    return sig

def nc_afsk2400Demod(sig, fc=3000, df=600, baud=2400, fs=48000, TBW=2):
    #  non-coherent demodulation of afsk1200
    # function returns the NRZ (without rectifying it)
    # 
    # sig  - signal
    # baud - The bitrate. Default 2400
    # fs   - sampling rate in Hz
    # TBW  - TBW product of the filters
    #
    # Returns:
    #     NRZ 
    t = np.linspace(0, TBW/baud*fs+1, TBW/baud*fs+1) 
    bpp = signal.firwin(TBW/baud*fs+1, 2*(2*df)/fs) * np.exp(2 * np.pi * 1j * (fc/fs)*t)
    bp1 = signal.firwin(TBW/baud*fs+1, 2*(baud)/fs) * np.exp(2 * np.pi * 1j* (fc - df)/fs*t)
    bp2 = signal.firwin(TBW/baud*fs+1, 2*(baud)/fs) * np.exp(2 * np.pi * 1j* (fc + df)/fs*t)
    lpf = signal.firwin(TBW/baud*fs+1, 2*(baud)/fs)
    sig = np.convolve(sig, bpp, mode="same")

    sig_mark = np.abs(np.convolve(sig, bp1, mode="same"))
    sig_space = np.abs(np.convolve(sig, bp2, mode="same"))
    nrz = np.convolve(-sig_mark + sig_space, lpf, mode="same") 
    return nrz

def PLL(NRZa, init_pll = 0, a = 0.74 , fs = 48000, baud = 2400): 
    # function implements a simple phased lock loop for tyming recovery
    #
    # Inputs:
    #          NRZa -   The NRZ signal
    #          a - nudge factor
    #          fs - sampling rate (arbitrary)
    #          baude  - the bit rate
    #
    # Outputs:
    # idx - array of indexes to sample at #
    tick = (2**32)//(fs//baud)
    pll = np.int32(init_pll)
    idx = []
    for i in range(0, len(NRZa)):
        if i != 0 and np.sign(NRZa[i-1]) != np.sign(NRZa[i]): #zero cros
            pll = np.int32(pll * a)
        tmp = pll
        pll = np.int32(np.int32(pll)+np.int32(tick)) 
        if (pll < tmp):
            idx.append(i) #this is a sample since the pll has overflowed
    bits =  bitarray.bitarray((NRZa[idx]>0).tolist())
    return bits, idx, pll

#expects the output DIRECTLY from the sd.playrec call
def decode_sig(rcvd_sig, preamble, fc=3000, df=600, baud=2400, fs=48000, TBW=2):
    #sig_rx = sig_rx.reshape(1, len(sig_rx))[0]
    nrz_sig1 = nc_afsk2400Demod(rcvd_sig, fc=fc, df=df, baud=baud, fs = fs)
    raw_bits, idx, pll = PLL(nrz_sig1, baud=baud, fs = fs)
    raw_bits = np.array(raw_bits, dtype = int)
    preamble_find = np.correlate(raw_bits,preamble, mode = 'valid')
    start = np.argmax(preamble_find)
    return raw_bits, start