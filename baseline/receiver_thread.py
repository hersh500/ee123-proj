import numpy as np
from numpy import r_
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure,plot, title,stem, loglog,xlabel,ylabel
import bitarray, time, urllib, ssl
from scipy import signal, integrate
#from math import gcd
from fractions import gcd
from functools import reduce
from numpy import ones,zeros, pi, cos, exp, sign
from scipy import misc
import serial
import sounddevice as sd
import threading
import time 
import queue
import modulation 

def cb_addToQueue(indata, outdata, frames, time, status):
    if status:
        print(status)
    outdata[:] = indata
    soundQueue.put(indata.copy()[:,0])
    
class stream_receiver(threading.Thread):
    def __init__(self, threadID, name, usb_idx, blocksize):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.usb_idx = usb_idx
        self.blocksize = blocksize #defines the number of samples to receive before doing a callback 
        self.exit_flag = 0
        self.st = sd.Stream(device=self.usb_idx,callback=cb_addToQueue, blocksize=self.blocksize)

    def run(self):
        print ("Starting " + self.name)
        self.receive_audio(self.name)
        print ("Exiting " + self.name)
        self.st.stop()
        self.st.close()

    def receive_audio(self, threadName):
        self.st.start()
        while True:
            if self.exit_flag == 1:
                self.st.stop()
                self.st.close()
                break
        return 
    
class demodder(threading.Thread):
    def __init__(self, threadID, name, bitsQueue, soundQueue, fc, df, baud, fs):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.exit_flag = 0
        self.bitsQueue = bitsQueue
        self.soundQueue = soundQueue
        self.fc = fc
        self.df = df
        self.baud = baud
        self.fs = fs
        
    def run(self):
        print ("Starting " + self.name)
        self.demod_audio(self.name)
        print ("Exiting " + self.name)
        return 
    
    def demod_audio(self, threadName):
        pll = 0 
        while not self.bitsQueue.full():
            if not self.soundQueue.empty():
                #print("Got audio chunk! demodding...")
                chunk = self.soundQueue.get()
                nrz = modulation.nc_afsk2400Demod(chunk, fc=self.fc, df=self.df, baud=self.baud, fs=self.fs)
                bits, idx, pll = modulation.PLL(nrz, pll, fs=self.fs, baud=self.baud)
                bits = np.array(bits, dtype = int)
                self.bitsQueue.put(bits) 
            if self.exit_flag == 1:
                break
        return 

#This tester class puts things into the audio Queue to test the demod-er               
class tester(threading.Thread):
    def __init__(self, threadID, name, audio_sig, blocksize):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.sig = audio_sig
        self.blocksize = blocksize
        
    def run(self):
        print ("Starting " + self.name)
        self.fill_sound_queue(self.name)
        print ("Exiting " + self.name)
        
    def fill_sound_queue(self, threadName):
        for i in range(self.blocksize, len(self.sig)+self.blocksize, self.blocksize):
            print(i)
            print("Tester is adding to sound Queue")
            soundQueue.put(self.sig[i-self.blocksize:min(i, len(self.sig))])
            time.sleep(0.1)
            
class transmitter(threading.Thread):
    def __init__(self, threadID, name, audio_sig, usb_idx, fs, blocksize):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.sig = audio_sig
        self.usb_idx = usb_idx
        self.fs = fs
        self.blocksize = blocksize
    def run(self):
        print ("Starting " + self.name)
        self.transmit_sound()
        print ("Exiting " + self.name)
        return 
    
    def transmit_sound(self):
        sd.play(self.sig, samplerate=self.fs, device=self.usb_idx, blocking=True)
        return 
            
#Note: Will empty the queue that gets passed in...
def serialize_Queue(q):
    sig = np.array([])
    while not q.empty():
        sig = np.append(sig, q.get())
    return sig

def main():
    audio_thread = stream_receiver(1, "stream", 1)
    demod_thread = demodder(2, "demodder", 2) 
    audio_thread.start()
    demod_thread.start()